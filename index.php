
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Resume - Mark Sobers</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="https://fonts.googleapis.com/css?family=Saira+Extra+Condensed:500,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Muli:400,400i,800,800i" rel="stylesheet">
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/resume.min.css" rel="stylesheet">


<body id="page-top">

  <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="sideNav">
    <a class="navbar-brand js-scroll-trigger" href="#page-top">
      <span class="d-block d-lg-none">Mark Sobers</span>
      <span class="d-none d-lg-block">
        <img class="img-fluid img-profile rounded-circle mx-auto mb-2" src="img/profile.jpg" alt="">
      </span>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#about">About</a>
        </li>
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#experience">Experience</a>
        </li>
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#education">Education</a>
        </li>
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#skills">Skills</a>
        </li>
      </ul>
    </div>
  </nav>

  <div class="container-fluid p-0">

    <section class="resume-section p-3 p-lg-5 d-flex align-items-center" id="about">
      <div class="w-100">
        <h1 class="mb-0">Mark
          <span class="text-primary">Sobers</span>
        </h1>
        <div class="subheading mb-5">37 Inner Bagotville · West Bank Demerara, 592 - 6044403
          <a href="mailto:mrksobers@email.com">mrksobers@email.com</a>
        </div>
        <p class="lead mb-5">Great admirer for web development, 4G, 5G and fiber Optics networks and great love for Open source GNU/Linux operating system.</p>
        <div class="social-icons">
          <a href="#">
            <i class="fab fa-linkedin-in"></i>
          </a>
          <a href="#">
            <i class="fab fa-google"></i>
          </a>
          <a href="#">
            <i class="fab fa-reddit"></i>
          </a>
          <a href="#">
            <i class="fab fa-facebook-f"></i>
          </a>
        </div>
      </div>
    </section>

    <hr class="m-0">

    <section class="resume-section p-3 p-lg-5 d-flex justify-content-center" id="experience">
      <div class="w-100">
        <h2 class="mb-5">Experience</h2>


        <div class="resume-item d-flex flex-column flex-md-row justify-content-between mb-5">
              <div class="resume-content">
            <h3 class="mb-0">NOC Technician</h3>
            <div class="subheading mb-3">Enetworks Inc </div>
            <p>Bring to the table Troubleshooting 4G, 5G and fiber Optic networks
              maintence and monitor.</p>
          </div>
          <div class="resume-date text-md-right">
            <span class="text-primary">August 2020 - October 2021</span>
          </div>
        </div>
        
        <div class="resume-item d-flex flex-column flex-md-row justify-content-between mb-5">
          <div class="resume-content">
            <h3 class="mb-0">IT Technician</h3>
            <div class="subheading mb-3">Ramada Princess Hotel Georgetown</div>
            <p>Bring to the table computer repairs knowledge, excellence
              troubleshooting skills, and maintence.</p>
          </div>
          <div class="resume-date text-md-right">
            <span class="text-primary">August 2019 - October 2019</span>
          </div>
        </div>

          

        <div class="resume-item d-flex flex-column flex-md-row justify-content-between mb-5">
          <div class="resume-content">
            <h3 class="mb-0">Data Inspector</h3>
            <div class="subheading mb-3">Public Procurement Commission</div>
            <p>Podcasting operational change management inside of workflows to establish a framework. Taking seamless key performance indicators offline to maximise the long tail. Keeping your eye on the ball while performing a deep dive on the start-up mentality to derive convergence on cross-platform integration.</p>
          </div>
          <div class="resume-date text-md-right">
            <span class="text-primary">March 2019 - May 2019</span>
          </div>
        </div>

        <div class="resume-item d-flex flex-column flex-md-row justify-content-between mb-5">
          <div class="resume-content">
            <h3 class="mb-0">Clerk II </h3>
            <div class="subheading mb-3">Guyana Revenue Authority</div>
            <p>Bring to the table strong customs and Trade knowledge, tax exemption knowledge
              and strong clerk skills to preform the task assign within
              a stipulated timeframe.</p>
          </div>
          <div class="resume-date text-md-right">
            <span class="text-primary">July 2009 - October 2017</span>
          </div>
        </div>

        <div class="resume-item d-flex flex-column flex-md-row justify-content-between">
          <div class="resume-content">
            <h3 class="mb-0">Poll Clerk and Ballot Clerk</h3>
            <div class="subheading mb-3">Guyana Election Commission</div>
            <p>Understand the electorial process within the laws of Guyana.</p>
          </div>
          <div class="resume-date text-md-right">
            <span class="text-primary"> June 2015, 2017</span>
          </div>
        </div>

        <div class="resume-item d-flex flex-column flex-md-row justify-content-between">
          <div class="resume-content">
            <h3 class="mb-0">Enumerator</h3>
            <div class="subheading mb-3">Bureau Statics of Guyana</div>
            <p>Collaboratively administrate empowered the national census of Guyana.</p>
          </div>
          <div class="resume-date text-md-right">
            <span class="text-primary">September 2012</span>
          </div>
        </div>

      </div>



    </section>

    <hr class="m-0">

    <section class="resume-section p-3 p-lg-5 d-flex align-items-center" id="education">
      <div class="w-100">
        <h2 class="mb-5">Education</h2>

        <div class="resume-item d-flex flex-column flex-md-row justify-content-between mb-5">
          <div class="resume-content">
            <h3 class="mb-0">University of Guyana</h3>
            <div class="subheading mb-3">Associate of Science</div>
            <div>Information Technology </div>
          </div>
          <div class="resume-date text-md-right">
            <span class="text-primary">August 2017 - Present</span>
          </div>
        </div>

        <div class="resume-item d-flex flex-column flex-md-row justify-content-between">
          <div class="resume-content">
            <h3 class="mb-0">Government Technical Instition</h3>
            <div class="subheading mb-3">Data Operations Level</div>
          </div>
          <div class="resume-date text-md-right">
            <span class="text-primary">September 2013 - May 2014</span>
          </div>
        </div>

      </div>
    </section>

    <hr class="m-0">

    <section class="resume-section p-3 p-lg-5 d-flex align-items-center" id="skills">
      <div class="w-100">
        <h2 class="mb-5">Skills</h2>

        <div class="subheading mb-3">Programming Languages &amp; Tools</div>
        <ul class="list-inline dev-icons">
          <li class="list-inline-item">
            <i class="fab fa-html5"></i>
          </li>
          <li class="list-inline-item">
            <i class="fab fa-css3-alt"></i>
          </li>
          <li class="list-inline-item">
            <i class="fab fa-js-square"></i>
          </li>
          <li class="list-inline-item">
            <i class="fab fa-linux"></i>
          </li>
          <li class="list-inline-item">
            <i class="fab fa-java"></i>
          </li>
          <li class="list-inline-item">
            <i class="fab fa-windows"></i>
          </li>
          <li class="list-inline-item">
            <i class="fab fa-sass"></i>
          </li>
          <li class="list-inline-item">
            <i class="fab fa-php"></i>
          </li>
          <li class="list-inline-item">
            <i class="fab fa-wordpress"></i>
          </li>
          <li class="list-inline-item">
            <i class="fab fa-github"></i>
          </li>
          <li class="list-inline-item">
            <i class="fab fa-gitlab"></i>
          </li>
        </ul>

        <div class="subheading mb-3">Workflow</div>
        <ul class="fa-ul mb-0">
          <i class="fa-li fa fa-check"></i>
          Agile Development &amp; Traditional</li>
        </ul>
      </div>
    </section>

    <hr class="m-0">

    <hr class="m-0">

    <section class="resume-section p-3 p-lg-5 d-flex align-items-center" id="awards">
      <div class="w-100">
        <h2 class="mb-5"> Certifications</h2>
        <ul class="fa-ul mb-0">
          <li>
            <i class="fa-li fa fa-trophy text-warning"></i>
            Caribbean Examinations Council</li>
          <li>

      </div>
    </section>

  </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/resume.min.js"></script>
  </footer>
</body>


</html>
